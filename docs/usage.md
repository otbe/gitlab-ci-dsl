# Usage

## Standalone

The simplest way to use the library is to create a `gitlab.main.kts` file in your project, make it executable and just
run it from your shell. This way you can use the full power of Kotlin to generate your CI file. You have to have
the `kotlin` executable on your `PATH` to make this work.

One of the downsides is you have to execute it manually after every change in order to reflect the changes in the
generated `.gitlab-ci.yml` file.

```kts
#!/usr/bin/env kotlin

@file:DependsOn("dev.otbe:gitlab-ci-dsl:0.2.0")

import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.dsl.*
import dev.otbe.gitlab.ci.core.goesTo

val pipe = pipeline {
    job("build") {
        stage = Stage("build")
        image = Image.of("alpine:latest")

        script("echo 'Hello World'")
    }
}

pipe goesTo ".gitlab-ci.yml"
```

## Integrated into maven

Alternatively, we provide a maven plugin that can be used to generate the `.gitlab-ci.yml` file during the build.
This way your pipeline definition is always up-to-date.

```xml
<plugin>
    <groupId>dev.otbe</groupId>
    <artifactId>gitlab-ci-maven-plugin</artifactId>
    <version>${version}</version>
    <executions>
        <execution>
            <goals>
                <goal>build</goal>
            </goals>
            <configuration>
                <inputFile>${project.basedir}/gitlab.main.kts</inputFile>
                <outputFile>${project.basedir}/.gitlab-ci.yml</outputFile>
            </configuration>
        </execution>
    </executions>
</plugin>
```

In your `gitlab.main.kts` file you should make sure that the last line is similar to this:

```kts
pipe.toYml()
```

So basically the generated yml file is written to the standard output. The maven plugin will read this output and write
it to `outputFile`.

See this projects `gitlab.main.kts` for an example.
