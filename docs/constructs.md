# Constructs

Constructs are higher level building blocks with sane defaults for certain use cases. Think of them in the same way
as [AWS CDK level 2/3 constructs](https://docs.aws.amazon.com/cdk/v2/guide/constructs.html).

In general all constructs behave like regular builders and can be used/customized in the same way.

## Pipelines

### DefaultPipeline

The `defaultPipeline` creates pipelines for `merge_requests` and the `default branch`.
By default, all jobs that you define will run for every merge request and for your default branch.
You can customize this by using rules to for example run certain jobs only on the default branch pipeline.

```kts
defaultPipeline {
    job("build") {
        // ...
    }

    job("deploy:PROD") {
        rules(Rules.ONLY_ON_DEFAULT_BRANCH)
        // ...
    }
}
```

Keep in mind rules are always evaluated in order and the first matching one wins. If no rule matches the job is not added to the pipeline.

## Jobs

### MvnJob

The `mvnJob` runs arbitrary maven commands on a java 17 image (`maven:3.9.1-eclipse-temurin-17"`) and also creates a
push-pull cache for all maven dependencies.

```kts
mvnJob("build", "verify")
```

Is equivalent to:

```kts
job("build") {
    image = Image.of("maven:3.9.1-eclipse-temurin-17")

    cache {
        policy = Cache.Policy.PULL_PUSH
        paths(Path(".m2/"))
    }

    script("mvn -Dmaven.repo.local=.m2/repository --batch-mode verify")
}
```

`mvnwJob` uses a different base image (`eclipse-temurin:17.0.7_7-jdk`) with no maven pre-installed and uses instead the
maven wrapper by executing `./mvnw`.

### CodecovJob

The `codecovJob` runs the [default script](https://docs.codecov.com/docs#step-4-upload-coverage-reports-to-codecov) for
uploading coverage reports to codecov.io.

Defining the job by just using `codecovJob()` inside your pipeline is enough. You still have to provide
a `$CODECOV_TOKEN` variable for your current project.