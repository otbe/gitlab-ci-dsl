# Gitlab CI DSL <!-- {docsify-ignore-all} -->

[![maven](https://img.shields.io/maven-central/v/dev.otbe/gitlab-ci)](https://central.sonatype.com/artifact/dev.otbe/gitlab-ci-dsl/) [![codecov](https://codecov.io/gl/otbe-dev/gitlab-ci-dsl/branch/main/graph/badge.svg?token=GGALTWMKVZ)](https://codecov.io/gl/otbe-dev/gitlab-ci-dsl)

Gitlab CI DSL is a Kotlin DSL for generating Gitlab CI yml files.

```kts
#!/usr/bin/env kotlin

@file:DependsOn("dev.otbe:gitlab-ci-dsl:0.2.0")

import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.dsl.*
import dev.otbe.gitlab.ci.core.goesTo

val pipe = pipeline {
    job("build") {
        image = Image.of("alpine:latest")

        script("echo 'Hello World'")
    }
}

pipe goesTo ".gitlab-ci.yml"
```

## Gitlab CI Coverage

This is a quite new project so not every feature of Gitlab CI is covered from the very beginning.
Other things are not covered intentionally like global variables or images.

If you need a feature that is not covered yet, please create an issue or even better a merge request. 😇

## Credits

This page is created with [docsify](https://docsify.js.org/)
and [docsify-themeable](https://jhildenbiddle.github.io/docsify-themeable/#/). These are awesome projects, please support
them.