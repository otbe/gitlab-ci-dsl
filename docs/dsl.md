# DSL

The DSL module is the basis for the whole library. It provides all building blocks to create a Gitlab CI pipeline.

```kts
@file:DependsOn("dev.otbe:gitlab-ci-dsl:<VERSION>")
```

## Basic Concepts

_Single value fields_ like `allow_failure`, `image`, `when` and friends are provided as mutable properties inside their
respective builders and can be (re) assigned at any time.

_Fields with multiple values_ like `rules`, `paths`, `tags` and others accept usually a list/varargs of value objects
and
in some cases also primitives like strings depending on the use case.

_More complex objects_ like `pipeline`, `job`, `artifact`, `cache` do provide a builder class which can be used to
create them inline
or for later use.

## Important Functions

### `pipeline`

Main building block. Also available as a builder `PipelineBuilder`.

```kts
val pipe = pipeline {
    include(Include.from("group/project", "project.gitlab-ci.yml"))

    workflow {}

    job("build") {
        // ...
    }

    job("test") {
        // ...
    }
}
```

A pipeline can be rendered to yaml by calling `pipe.toYml()`.
There's also a convenient function to write the pipeline directly to a file: `pipe goesTo ".gitlab-ci.yml"`.

### `job`

Jobs can be created inside a pipeline or via the `JobBuilder`.
They can be used as values for `needs`.

```kts
pipeline {
    val build = job("build") {
        // ...
        artifacts {
            paths("my-artifact.zip")
        }
    }
    
    job("deploy") {
        needs(build)
        // ...
    }
}
```

### `artifacts`

Can be created inside a job or via the `ArtifactsBuilder` for later use.

```kts
pipeline {
    job("build") {
        // ...
        artifacts {
            expireIn = 1.days
            
            paths("my-artifact.zip")
        }
    }
}
```

### `cache`

Can be created inside a job or via the `CacheBuilder` for later use.

```kts
pipeline {
    job("build") {
        // ...
        cache {
            `when` = When.ALWAYS
            paths("node_modules")
        }
    }
}
```