# Gitlab CI DSL

An easy to use kotlin based DSL for creating Gitlab CI definitions. 

[Documentation page](https://otbe-dev.gitlab.io/gitlab-ci-dsl)

## Primer

```kotlin
#!/usr/bin/env kotlin

@file:DependsOn("dev.otbe:gitlab-ci-dsl:0.2.0")

import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.dsl.*
import dev.otbe.gitlab.ci.core.goesTo

val pipe = pipeline {
    job("build") {
        image = Image.of("alpine:latest")

        script("echo 'Hello World'")
    }
}

pipe goesTo ".gitlab-ci.yml"
```

## Roadmap

- [x] low level DSL
- [x] (mostly) Feature parity with Gitlab CI
- [x] Tests 🥳
- [x] Documentation
- [x] (some) higher level abstractions (similar to what CDK does)
- [x] maven plugin for easier integration
- [x] 0.1.0 release
- [ ] gradle plugin for easier integration
- [ ] (more) higher level abstractions (similar to what CDK does)

## Releasing a new version

- `mvn gitflow:release`
- select version
- checkout new tag
- `mvn -Dossrh deploy`