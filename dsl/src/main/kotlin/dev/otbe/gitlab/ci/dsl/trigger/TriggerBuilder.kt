package dev.otbe.gitlab.ci.dsl.trigger

import dev.otbe.gitlab.ci.core.model.Trigger
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker

@GitlabCiDslMarker
open class TriggerBuilder(setup: TriggerBuilder.() -> Unit = {}) : Builder<Trigger> {
    var project: String? = null
    var branch: String? = null
    var strategy: Trigger.Strategy? = null
    var forward: Trigger.Forward? = null

    init {
        setup()
    }

    override fun build(): Trigger {
        return Trigger(
            project,
            branch,
            strategy,
            forward,
        )
    }
}
