package dev.otbe.gitlab.ci.dsl.rules

import dev.otbe.gitlab.ci.core.model.Rule

class RulesAwareness : RulesAware {
    internal val rules: MutableList<Rule> = mutableListOf()

    override fun rules(init: RulesBuilder.() -> Unit) {
        rules(RulesBuilder().apply(init).build())
    }

    override fun rules(rules: List<Rule>) {
        this.rules.clear()
        this.rules += rules
    }

    override fun rules(vararg rule: Rule) {
        rules(rule.asList())
    }
}
