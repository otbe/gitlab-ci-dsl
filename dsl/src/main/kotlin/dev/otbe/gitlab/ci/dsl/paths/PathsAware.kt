package dev.otbe.gitlab.ci.dsl.paths

import dev.otbe.gitlab.ci.core.model.Path

interface PathsAware {
    /**
     * Configures a list of paths to be used in cache, artifacts or rules.
     */
    fun paths(paths: List<Path>)

    /**
     * Configures a list of paths to be used in cache, artifacts or rules.
     */
    fun paths(vararg path: Path)

    /**
     * Configures a list of paths to be used in cache, artifacts or rules.
     */
    @Suppress("INAPPLICABLE_JVM_NAME")
    @JvmName("pathsAsString")
    fun paths(paths: List<String>)

    /**
     * Configures a list of paths to be used in cache, artifacts or rules.
     */
    fun paths(vararg path: String)
}
