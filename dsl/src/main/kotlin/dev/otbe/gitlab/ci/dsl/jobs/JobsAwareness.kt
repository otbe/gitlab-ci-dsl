package dev.otbe.gitlab.ci.dsl.jobs

import dev.otbe.gitlab.ci.core.model.Job

class JobsAwareness : JobsAware {
    internal val jobs: MutableList<Job> = mutableListOf()

    /**
     * Adds a pre-existing job to the pipeline.
     */
    override fun job(job: Job) {
        jobs += job
    }

    /**
     * Creates a new job, adds it to the pipeline and returns it.
     */
    override fun job(name: String, init: JobBuilder.() -> Unit): Job {
        val job = JobBuilder(name).apply(init).build()

        job(job)

        return job
    }
}
