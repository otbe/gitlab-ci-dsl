package dev.otbe.gitlab.ci.dsl.environment

import dev.otbe.gitlab.ci.core.model.Environment

interface EnvironmentAware {
    /**
     * Sets a new environment section. Can also be used to reset it when `null` is passed.
     */
    fun environment(environment: Environment?)

    /**
     * Configures a new environment section via its builder.
     * @see [EnvironmentBuilder]
     */
    fun environment(name: String, init: EnvironmentBuilder.() -> Unit = {})
}
