package dev.otbe.gitlab.ci.dsl.paths

import dev.otbe.gitlab.ci.core.model.Path

class PathsAwareness : PathsAware {
    internal val paths: MutableList<Path> = mutableListOf()

    override fun paths(paths: List<Path>) {
        this.paths.clear()
        this.paths += paths
    }

    override fun paths(vararg path: Path) {
        paths(path.asList())
    }

    @Suppress("INAPPLICABLE_JVM_NAME")
    @JvmName("pathsAsString")
    override fun paths(paths: List<String>) {
        paths(paths.map { Path(it) })
    }

    override fun paths(vararg path: String) {
        paths(path.asList())
    }
}
