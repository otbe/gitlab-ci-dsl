package dev.otbe.gitlab.ci.dsl.workflow

import dev.otbe.gitlab.ci.core.model.Rule
import dev.otbe.gitlab.ci.core.model.Workflow

class WorkflowAwareness : WorkflowAware {
    internal var workflow: Workflow? = null

    override fun workflow(init: WorkflowBuilder.() -> Unit) {
        workflow(WorkflowBuilder().apply(init).build())
    }

    override fun workflow(workflow: Workflow?) {
        this.workflow = workflow
    }

    override fun workflow(rules: List<Rule>) {
        workflow(Workflow(rules = rules))
    }
}
