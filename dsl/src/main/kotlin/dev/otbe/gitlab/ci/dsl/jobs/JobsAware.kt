package dev.otbe.gitlab.ci.dsl.jobs

import dev.otbe.gitlab.ci.core.model.Job

interface JobsAware {
    /**
     * Adds a job to the list of jobs.
     */
    fun job(job: Job)

    /**
     * Configures a new job via its builder.
     * @see [JobBuilder]
     */
    fun job(name: String, init: JobBuilder.() -> Unit = {}): Job
}
