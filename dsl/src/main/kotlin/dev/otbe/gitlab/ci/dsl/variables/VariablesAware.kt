package dev.otbe.gitlab.ci.dsl.variables

interface VariablesAware {
    /**
     * Configures a list of variables to be used in the job.
     */
    fun variables(vararg entries: Pair<String, String>)

    /**
     * Configures a list of variables to be used in the job.
     */
    fun variables(vars: Map<String, String>)
}
