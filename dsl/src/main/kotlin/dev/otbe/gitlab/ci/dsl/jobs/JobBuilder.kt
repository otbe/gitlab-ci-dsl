package dev.otbe.gitlab.ci.dsl.jobs

import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker
import dev.otbe.gitlab.ci.dsl.artifacts.ArtifactsAware
import dev.otbe.gitlab.ci.dsl.artifacts.ArtifactsAwareness
import dev.otbe.gitlab.ci.dsl.cache.CacheAware
import dev.otbe.gitlab.ci.dsl.cache.CacheAwareness
import dev.otbe.gitlab.ci.dsl.environment.EnvironmentAware
import dev.otbe.gitlab.ci.dsl.environment.EnvironmentAwareness
import dev.otbe.gitlab.ci.dsl.needs.NeedsAware
import dev.otbe.gitlab.ci.dsl.needs.NeedsAwareness
import dev.otbe.gitlab.ci.dsl.rules.RulesAware
import dev.otbe.gitlab.ci.dsl.rules.RulesAwareness
import dev.otbe.gitlab.ci.dsl.scripts.ScriptsAware
import dev.otbe.gitlab.ci.dsl.scripts.ScriptsAwareness
import dev.otbe.gitlab.ci.dsl.tags.TagsAware
import dev.otbe.gitlab.ci.dsl.tags.TagsAwareness
import dev.otbe.gitlab.ci.dsl.trigger.TriggerAware
import dev.otbe.gitlab.ci.dsl.trigger.TriggerAwareness
import dev.otbe.gitlab.ci.dsl.variables.VariablesAware
import dev.otbe.gitlab.ci.dsl.variables.VariablesAwareness
import kotlin.time.Duration

@GitlabCiDslMarker
open class JobBuilder(
    private val name: String,
    private val needsAwareness: NeedsAwareness = NeedsAwareness(),
    private val rulesAwareness: RulesAwareness = RulesAwareness(),
    private val scriptsAwareness: ScriptsAwareness = ScriptsAwareness(),
    private val tagsAwareness: TagsAwareness = TagsAwareness(),
    private val artifactsAwareness: ArtifactsAwareness = ArtifactsAwareness(),
    private val cacheAwareness: CacheAwareness = CacheAwareness(),
    private val variablesAwareness: VariablesAwareness = VariablesAwareness(),
    private val environmentAwareness: EnvironmentAwareness = EnvironmentAwareness(),
    private val triggerAwareness: TriggerAwareness = TriggerAwareness(),
    setup: JobBuilder.() -> Unit = {},
) : Builder<Job>,
    RulesAware by rulesAwareness,
    NeedsAware by needsAwareness,
    ScriptsAware by scriptsAwareness,
    CacheAware by cacheAwareness,
    ArtifactsAware by artifactsAwareness,
    TagsAware by tagsAwareness,
    VariablesAware by variablesAwareness,
    EnvironmentAware by environmentAwareness,
    TriggerAware by triggerAwareness {

    var stage: Stage = Stage(name)
    var image: Image? = null
    var interruptible: Boolean? = false
    var allowFailure: Boolean? = false
    var resourceGroup: ResourceGroup? = null
    var timeout: Duration? = null
    var coverage: String? = null

    init {
        setup()
    }

    override fun build(): Job {
        val trigger = triggerAwareness.trigger
        val scripts = scriptsAwareness.scripts

        if (trigger == null && scripts.isEmpty()) {
            throw Exception("$name does not have a script section")
        }

        if (trigger != null && scripts.isNotEmpty()) {
            throw Exception("$name has both a trigger and a script section")
        }

        // more checks for trigger vs ordinary job

        return Job(
            name,
            stage,
            image,
            variablesAwareness.variables,
            needsAwareness.needs,
            artifactsAwareness.artifacts,
            rulesAwareness.rules,
            scripts,
            cacheAwareness.cache,
            coverage,
            interruptible,
            allowFailure,
            timeout,
            resourceGroup,
            tagsAwareness.tags,
            environmentAwareness.environment,
            trigger,
        )
    }
}
