package dev.otbe.gitlab.ci.dsl.scripts

import org.intellij.lang.annotations.Language

interface ScriptsAware {
    /**
     * Configures a single script to be executed. Best to use a multiline string.
     */
    fun script(@Language("Shell Script") script: String)

    /**
     * Configures a list of scripts to be executed.
     */
    fun scripts(scripts: List<String>)

    /**
     * Configures a list of scripts to be executed.
     */
    fun scripts(vararg scripts: String)
}
