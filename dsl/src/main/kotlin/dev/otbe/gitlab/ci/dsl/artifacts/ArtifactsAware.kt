package dev.otbe.gitlab.ci.dsl.artifacts

import dev.otbe.gitlab.ci.core.model.Artifacts

interface ArtifactsAware {
    /**
     * Configures a new artifacts section via its builder.
     * @see [ArtifactsBuilder]
     */
    fun artifacts(init: ArtifactsBuilder.() -> Unit)

    /**
     * Sets a new artifacts section. Can also be used to reset it when `null` is passed.
     */
    fun artifacts(artifacts: Artifacts?)
}
