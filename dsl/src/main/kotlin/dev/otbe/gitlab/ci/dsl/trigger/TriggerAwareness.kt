package dev.otbe.gitlab.ci.dsl.trigger

import dev.otbe.gitlab.ci.core.model.Trigger

class TriggerAwareness : TriggerAware {
    internal var trigger: Trigger? = null

    override fun trigger(init: TriggerBuilder.() -> Unit) {
        trigger(TriggerBuilder().apply(init).build())
    }

    override fun trigger(trigger: Trigger?) {
        this.trigger = trigger
    }
}
