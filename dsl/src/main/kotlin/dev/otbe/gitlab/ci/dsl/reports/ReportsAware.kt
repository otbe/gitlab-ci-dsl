package dev.otbe.gitlab.ci.dsl.reports

import dev.otbe.gitlab.ci.core.model.Artifacts

interface ReportsAware {
    fun reports(reports: List<Artifacts.Report>)
    fun reports(vararg report: Artifacts.Report)
}
