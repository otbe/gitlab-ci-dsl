package dev.otbe.gitlab.ci.dsl.cache

import dev.otbe.gitlab.ci.core.model.Cache
import dev.otbe.gitlab.ci.core.model.CacheKey
import dev.otbe.gitlab.ci.core.model.When
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker
import dev.otbe.gitlab.ci.dsl.paths.PathsAware
import dev.otbe.gitlab.ci.dsl.paths.PathsAwareness

@GitlabCiDslMarker
open class CacheBuilder(
    private val pathsAwareness: PathsAwareness = PathsAwareness(),
    setup: CacheBuilder.() -> Unit = {},
) :
    Builder<Cache>, PathsAware by pathsAwareness {

    var `when`: When? = null

    var key: CacheKey? = null

    var untracked: Boolean? = null

    var unprotected: Boolean? = null

    var policy: Cache.Policy? = null

    var fallbackKeys: List<String>? = emptyList()

    init {
        setup()
    }

    override fun build(): Cache {
        return Cache(pathsAwareness.paths, key, untracked, unprotected, `when`, policy, fallbackKeys)
    }
}
