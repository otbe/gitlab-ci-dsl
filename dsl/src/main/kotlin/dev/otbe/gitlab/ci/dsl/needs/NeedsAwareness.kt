package dev.otbe.gitlab.ci.dsl.needs

import dev.otbe.gitlab.ci.core.model.Job

class NeedsAwareness : NeedsAware {
    internal val needs: MutableList<String> = mutableListOf()

    override fun needs(vararg job: Job) {
        needs(job.asList())
    }

    override fun needs(jobs: List<Job>) {
        needs(jobs.map { it.name })
    }

    override fun needs(vararg job: String) {
        needs(job.asList())
    }

    @Suppress("INAPPLICABLE_JVM_NAME")
    @JvmName("needsAsString")
    override fun needs(jobs: List<String>) {
        needs.clear()
        needs += jobs
    }
}
