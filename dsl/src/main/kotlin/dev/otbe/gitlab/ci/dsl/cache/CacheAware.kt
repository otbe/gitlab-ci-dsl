package dev.otbe.gitlab.ci.dsl.cache

import dev.otbe.gitlab.ci.core.model.Cache

interface CacheAware {
    /**
     * Adds a list of tags to the job.
     */
    fun cache(caches: List<Cache>)

    /**
     * Sets a new cache section. Can also be used to reset it when `null` is passed.
     */
    fun cache(vararg cache: Cache)

    /**
     * Configures a new cache section via its builder.
     * @see [CacheBuilder]
     */
    fun cache(init: CacheBuilder.() -> Unit)
}
