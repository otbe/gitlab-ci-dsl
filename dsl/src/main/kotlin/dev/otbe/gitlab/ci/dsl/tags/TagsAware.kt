package dev.otbe.gitlab.ci.dsl.tags

import dev.otbe.gitlab.ci.core.model.Tag

interface TagsAware {
    /**
     * Adds tags to the job.
     */
    fun tags(vararg tag: Tag)

    /**
     * Adds a list of tags to the job.
     */
    fun tags(tags: List<Tag>)

    /**
     * Adds tags to the job.
     */
    fun tags(vararg tag: String)

    /**
     * Adds a list of tags to the job.
     */
    @Suppress("INAPPLICABLE_JVM_NAME")
    @JvmName("tagsAsList")
    fun tags(tags: List<String>)
}
