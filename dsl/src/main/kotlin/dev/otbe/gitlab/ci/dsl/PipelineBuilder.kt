package dev.otbe.gitlab.ci.dsl

import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.dsl.jobs.JobsAware
import dev.otbe.gitlab.ci.dsl.jobs.JobsAwareness
import dev.otbe.gitlab.ci.dsl.workflow.WorkflowAware
import dev.otbe.gitlab.ci.dsl.workflow.WorkflowAwareness

@GitlabCiDslMarker
open class PipelineBuilder(
    private val workflowAwareness: WorkflowAwareness = WorkflowAwareness(),
    private val jobsAwareness: JobsAwareness = JobsAwareness(),
    setup: PipelineBuilder.() -> Unit = {},
) : Builder<Pipeline>, WorkflowAware by workflowAwareness, JobsAware by jobsAwareness {
    private var includes: List<Include> = emptyList()
    private var stages: List<Stage> = emptyList()

    init {
        setup()
    }

    /**
     * Configures the include section of the pipeline.
     */
    fun include(vararg include: Include) {
        includes = include.asList()
    }

    /**
     * Configures the stages section of the pipeline. Usually you don't need to use this method because the stages are deduced from the jobs.
     */
    fun stages(vararg stage: Stage) {
        stages = stage.asList()
    }

    override fun build() = Pipeline(
        includes,
        workflowAwareness.workflow,
        null,
        stages.ifEmpty { jobsAwareness.jobs.map { it.stage }.distinct() },
        jobsAwareness.jobs,
    )
}

fun pipeline(init: PipelineBuilder.() -> Unit): Pipeline =
    PipelineBuilder().apply(init).build()
