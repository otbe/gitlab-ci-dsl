package dev.otbe.gitlab.ci.dsl.artifacts

import dev.otbe.gitlab.ci.core.model.Artifacts

class ArtifactsAwareness : ArtifactsAware {
    internal var artifacts: Artifacts? = null

    override fun artifacts(init: ArtifactsBuilder.() -> Unit) {
        artifacts(ArtifactsBuilder().apply(init).build())
    }

    override fun artifacts(artifacts: Artifacts?) {
        this.artifacts = artifacts
    }
}
