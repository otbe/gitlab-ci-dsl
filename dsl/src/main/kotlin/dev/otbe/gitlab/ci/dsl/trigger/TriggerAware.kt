package dev.otbe.gitlab.ci.dsl.trigger

import dev.otbe.gitlab.ci.core.model.Trigger

interface TriggerAware {
    /**
     * Configure the trigger for this job via its builder.
     */
    fun trigger(init: TriggerBuilder.() -> Unit)

    /**
     * Set the trigger for this job. Can be used to reset the trigger.
     */
    fun trigger(trigger: Trigger?)
}
