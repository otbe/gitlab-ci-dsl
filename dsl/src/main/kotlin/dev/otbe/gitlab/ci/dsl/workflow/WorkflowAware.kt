package dev.otbe.gitlab.ci.dsl.workflow

import dev.otbe.gitlab.ci.core.model.Rule
import dev.otbe.gitlab.ci.core.model.Workflow

interface WorkflowAware {
    /**
     * Configures the workflow of the pipeline via its builder
     */
    fun workflow(init: WorkflowBuilder.() -> Unit)

    /**
     *  Configures the workflow of the pipeline via a simple list of rules to be used.
     */
    fun workflow(rules: List<Rule>)

    /**
     *  Configures the workflow of the pipeline. Can be used to reset the workflow.
     */
    fun workflow(workflow: Workflow?)
}
