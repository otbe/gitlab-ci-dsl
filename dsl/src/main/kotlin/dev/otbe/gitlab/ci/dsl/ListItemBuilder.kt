package dev.otbe.gitlab.ci.dsl

abstract class ListItemBuilder<T> : Builder<List<T>> {
    protected val items: MutableList<T> = mutableListOf()

    override fun build(): List<T> = items
}
