package dev.otbe.gitlab.ci.dsl.variables

class VariablesAwareness : VariablesAware {
    internal val variables: MutableMap<String, String> = mutableMapOf()

    override fun variables(vararg entries: Pair<String, String>) {
        variables(entries.toMap())
    }

    override fun variables(vars: Map<String, String>) {
        variables.clear()
        variables += vars
    }
}
