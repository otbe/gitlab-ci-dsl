package dev.otbe.gitlab.ci.dsl.environment

import dev.otbe.gitlab.ci.core.model.Environment

class EnvironmentAwareness : EnvironmentAware {
    internal var environment: Environment? = null

    override fun environment(environment: Environment?) {
        this.environment = environment
    }

    override fun environment(name: String, init: EnvironmentBuilder.() -> Unit) {
        environment(EnvironmentBuilder(name).apply(init).build())
    }
}
