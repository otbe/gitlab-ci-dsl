package dev.otbe.gitlab.ci.dsl.artifacts

import dev.otbe.gitlab.ci.core.model.Artifacts
import dev.otbe.gitlab.ci.core.model.When
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker
import dev.otbe.gitlab.ci.dsl.paths.PathsAware
import dev.otbe.gitlab.ci.dsl.paths.PathsAwareness
import dev.otbe.gitlab.ci.dsl.reports.ReportsAware
import dev.otbe.gitlab.ci.dsl.reports.ReportsAwareness
import kotlin.time.Duration

@GitlabCiDslMarker
open class ArtifactsBuilder(
    private val pathsAwareness: PathsAwareness = PathsAwareness(),
    private val reportsAwareness: ReportsAwareness = ReportsAwareness(),
    setup: ArtifactsBuilder.() -> Unit = {},
) : Builder<Artifacts>, PathsAware by pathsAwareness, ReportsAware by reportsAwareness {
    var name: String? = null
    var public: Boolean? = null
    var expireIn: Duration? = null
    var exposeAs: String? = null
    var untracked: Boolean? = null
    var `when`: When? = null

    init {
        setup()
    }

    override fun build(): Artifacts {
        return Artifacts(
            name,
            public,
            expireIn,
            exposeAs,
            pathsAwareness.paths,
            reportsAwareness.reports,
            untracked,
            `when`,
        )
    }
}
