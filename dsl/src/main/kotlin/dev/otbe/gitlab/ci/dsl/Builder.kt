package dev.otbe.gitlab.ci.dsl

interface Builder<T> {
    fun build(): T
}
