package dev.otbe.gitlab.ci.dsl.rules

import dev.otbe.gitlab.ci.core.model.Rule
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker
import dev.otbe.gitlab.ci.dsl.ListItemBuilder
import dev.otbe.gitlab.ci.dsl.paths.PathsAware
import dev.otbe.gitlab.ci.dsl.paths.PathsAwareness
import dev.otbe.gitlab.ci.dsl.variables.VariablesAware
import dev.otbe.gitlab.ci.dsl.variables.VariablesAwareness

@GitlabCiDslMarker
open class RulesBuilder(setup: RulesBuilder.() -> Unit = {}) : ListItemBuilder<Rule>() {
    init {
        setup()
    }

    operator fun Rule.unaryPlus() {
        this@RulesBuilder.items.add(this)
    }

    operator fun List<Rule>.unaryPlus() {
        this@RulesBuilder.items.addAll(this)
    }

    /**
     * Creates a [Rule] object via its builder DSL.
     */
    fun rule(init: RuleBuilder.() -> Unit): Rule {
        val builder = RuleBuilder()

        builder.init()

        return builder.build()
    }

    override fun build(): List<Rule> {
        return items
    }
}

@GitlabCiDslMarker
open class RuleBuilder(
    private val variablesAwareness: VariablesAwareness = VariablesAwareness(),
    setup: RuleBuilder.() -> Unit = {},
) : VariablesAware by variablesAwareness {
    var `if`: String? = null
    var exists: List<String> = emptyList()
    var allowFailure: Boolean? = null
    var `when`: Rule.When? = null

    private var changes: Rule.Change? = null

    init {
        setup()
    }

    fun changes(init: ChangeBuilder.() -> Unit) {
        val builder = ChangeBuilder()

        builder.init()

        changes = builder.build()
    }

    fun build(): Rule {
        return Rule(`if`, changes, exists, allowFailure, variablesAwareness.variables, `when`)
    }
}

@GitlabCiDslMarker
open class ChangeBuilder(private val pathsAwareness: PathsAwareness = PathsAwareness()) :
    Builder<Rule.Change>, PathsAware by pathsAwareness {

    var compareTo: List<String> = emptyList()

    override fun build(): Rule.Change {
        if (pathsAwareness.paths.isEmpty()) {
            throw Exception("every change needs a path")
        }

        return Rule.Change(pathsAwareness.paths, compareTo)
    }
}
