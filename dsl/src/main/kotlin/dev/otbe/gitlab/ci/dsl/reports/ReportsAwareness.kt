package dev.otbe.gitlab.ci.dsl.reports

import dev.otbe.gitlab.ci.core.model.Artifacts

class ReportsAwareness : ReportsAware {
    internal val reports: MutableList<Artifacts.Report> = mutableListOf()

    /**
     * Adds a list if reports to the artifacts configuration.
     */
    override fun reports(reports: List<Artifacts.Report>) {
        this.reports.clear()
        this.reports += reports
    }

    /**
     * Adds a list if reports to the artifacts configuration.
     */
    override fun reports(vararg report: Artifacts.Report) {
        reports(report.asList())
    }
}
