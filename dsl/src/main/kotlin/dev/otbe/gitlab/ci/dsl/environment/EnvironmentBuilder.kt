package dev.otbe.gitlab.ci.dsl.environment

import dev.otbe.gitlab.ci.core.model.Environment
import dev.otbe.gitlab.ci.core.model.Job
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker
import kotlin.time.Duration

@GitlabCiDslMarker
open class EnvironmentBuilder(
    private val name: String,
    setup: EnvironmentBuilder.() -> Unit = {},
) : Builder<Environment> {
    var url: String? = null
    var onStop: String? = null
    var action: Environment.Action? = null
    var autoStopIn: Duration? = null
    var deploymentTier: Environment.DeploymentTier? = null

    init {
        setup()
    }

    fun onStop(job: Job) {
        onStop = job.name
    }

    override fun build(): Environment {
        return Environment(
            name,
            url,
            onStop,
            action,
            autoStopIn,
            deploymentTier,
        )
    }
}
