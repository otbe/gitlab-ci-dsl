package dev.otbe.gitlab.ci.dsl.rules

import dev.otbe.gitlab.ci.core.model.Rule

interface RulesAware {
    /**
     * Configures a list of rules to be used in jobs or the workflow via its builder
     */
    fun rules(init: RulesBuilder.() -> Unit)

    /**
     * Configures a list of rules to be used in jobs or the workflow.
     */
    fun rules(rules: List<Rule>)

    /**
     * Configures a list of rules to be used in jobs or the workflow.
     */
    fun rules(vararg rule: Rule)
}
