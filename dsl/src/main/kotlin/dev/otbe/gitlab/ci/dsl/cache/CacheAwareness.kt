package dev.otbe.gitlab.ci.dsl.cache

import dev.otbe.gitlab.ci.core.model.Cache

class CacheAwareness : CacheAware {
    internal var cache: MutableList<Cache> = mutableListOf()

    override fun cache(caches: List<Cache>) {
        this.cache.clear()
        this.cache += caches
    }

    override fun cache(vararg cache: Cache) {
        cache(cache.asList())
    }

    override fun cache(init: CacheBuilder.() -> Unit) {
        cache(CacheBuilder().apply(init).build())
    }
}
