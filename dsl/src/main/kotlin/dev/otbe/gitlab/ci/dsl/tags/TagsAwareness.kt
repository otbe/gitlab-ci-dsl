package dev.otbe.gitlab.ci.dsl.tags

import dev.otbe.gitlab.ci.core.model.Tag

class TagsAwareness : TagsAware {
    internal val tags: MutableList<Tag> = mutableListOf()

    override fun tags(vararg tag: Tag) {
        tags(tag.asList())
    }

    override fun tags(tags: List<Tag>) {
        this.tags.clear()
        this.tags += tags
    }

    override fun tags(vararg tag: String) {
        tags(tag.asList())
    }

    @Suppress("INAPPLICABLE_JVM_NAME")
    @JvmName("tagsAsList")
    override fun tags(tags: List<String>) {
        tags(tags.map { Tag(it) })
    }
}
