package dev.otbe.gitlab.ci.dsl.needs

import dev.otbe.gitlab.ci.core.model.Job

interface NeedsAware {
    /**
     * Adds a dependency to the job.
     */
    fun needs(vararg job: Job)

    /**
     * Adds a list of jobs as dependencies to the job.
     */
    fun needs(jobs: List<Job>)

    /**
     * Adds a dependency to the job via its name.
     */
    fun needs(vararg job: String)

    /**
     * Adds a list of jobs as dependencies to the job via their names.
     */
    @Suppress("INAPPLICABLE_JVM_NAME")
    @JvmName("needsAsString")
    fun needs(jobs: List<String>)
}
