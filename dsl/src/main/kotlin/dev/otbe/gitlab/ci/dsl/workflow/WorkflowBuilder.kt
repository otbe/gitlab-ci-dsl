package dev.otbe.gitlab.ci.dsl.workflow

import dev.otbe.gitlab.ci.core.model.Workflow
import dev.otbe.gitlab.ci.dsl.Builder
import dev.otbe.gitlab.ci.dsl.GitlabCiDslMarker
import dev.otbe.gitlab.ci.dsl.rules.RulesAware
import dev.otbe.gitlab.ci.dsl.rules.RulesAwareness

@GitlabCiDslMarker
open class WorkflowBuilder(
    private val rulesAwareness: RulesAwareness = RulesAwareness(),
    setup: WorkflowBuilder.() -> Unit = {},
) : Builder<Workflow>, RulesAware by rulesAwareness {
    var name: String? = null

    init {
        setup()
    }

    override fun build(): Workflow {
        if (rulesAwareness.rules.isEmpty()) {
            throw Exception("workflow must have rules")
        }

        return Workflow(name, rulesAwareness.rules)
    }
}
