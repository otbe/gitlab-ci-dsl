package dev.otbe.gitlab.ci.dsl.scripts

class ScriptsAwareness : ScriptsAware {
    internal val scripts: MutableList<String> = mutableListOf()

    override fun script(script: String) {
        scripts(script)
    }

    override fun scripts(scripts: List<String>) {
        this.scripts.clear()
        this.scripts += scripts
    }

    override fun scripts(vararg scripts: String) {
        scripts(scripts.asList())
    }
}
