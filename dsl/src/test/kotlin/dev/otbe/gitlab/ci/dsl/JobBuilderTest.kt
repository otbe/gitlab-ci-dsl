package dev.otbe.gitlab.ci.dsl

import dev.otbe.gitlab.ci.core.model.Job
import dev.otbe.gitlab.ci.core.model.Stage
import dev.otbe.gitlab.ci.dsl.jobs.JobBuilder
import org.junit.jupiter.api.Test
import strikt.api.expectCatching
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isFailure
import strikt.assertions.isSuccess

class JobBuilderTest {
    @Test
    fun `defaults to job name for stage`() {
        val job = JobBuilder("foo") {
            script("foo")
        }.build()

        expectThat(job).with(Job::stage) {
            isEqualTo(Stage("foo"))
        }
    }

    @Test
    fun `no script set`() {
        expectCatching {
            JobBuilder("foo") {
                stage = Stage("foo")
            }.build()
        }
            .isFailure()
            .with(Throwable::message) {
                isEqualTo("foo does not have a script section")
            }
    }

    @Test
    fun `no script set for trigger`() {
        expectCatching {
            JobBuilder("foo") {
                stage = Stage("foo")

                trigger {
                    project = "bar"
                }
            }.build()
        }.isSuccess()
    }

    @Test
    fun `trigger and scripts set`() {
        expectCatching {
            JobBuilder("foo") {
                stage = Stage("foo")

                script("foo")

                trigger {
                    project = "bar"
                }
            }.build()
        }
            .isFailure()
            .with(Throwable::message) {
                isEqualTo("foo has both a trigger and a script section")
            }
    }
}
