package dev.otbe.gitlab.ci.dsl

import au.com.origin.snapshots.Expect
import au.com.origin.snapshots.junit5.SnapshotExtension
import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.core.toYml
import dev.otbe.gitlab.ci.dsl.cache.CacheBuilder
import dev.otbe.gitlab.ci.dsl.jobs.JobBuilder
import dev.otbe.gitlab.ci.dsl.rules.RuleBuilder
import dev.otbe.gitlab.ci.dsl.rules.RulesBuilder
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

@ExtendWith(SnapshotExtension::class)
class DslTest {
    private lateinit var expect: Expect

    @Test
    fun completeExample() {
        val setupStage = Stage("setup")
        val buildStage = Stage("build")
        val reportStage = Stage("report")

        val ownRunner = Tag.of("bar")

        val onlyOnMrRule = RuleBuilder {
            `if` = "\$CI_PIPELINE_SOURCE == \"merge_request_event\""
        }.build()

        val onlyOnMrsRules = RulesBuilder {
            +onlyOnMrRule
        }.build()

        val setup = JobBuilder("setup") {
            stage = setupStage

            cache {
                paths(".m2/repository")

                unprotected = true
                fallbackKeys = listOf("default")

                key = CacheKey.of("foo")
            }

            needs("bar")

            tags(ownRunner)

            rules {
                +rule {
                    `if` = "\$CI_PIPELINE_SOURCE == \"merge_request_event\""
                }
            }

            scripts("foo", "bar")
        }.build()

        val mvnCache = CacheBuilder {
            policy = Cache.Policy.PULL
            paths(Path(".m2/repository"))
        }.build()

        val pipe = pipeline {
            include(Include.from("foo/bar", "baz.yml"))

            workflow {
                rules {
                    +rule {
                        `if` = "\$CI_PIPELINE_SOURCE == \"merge_request_event\""
                    }
                }
            }

            job(setup)

            val unit = job("unit") {
                stage = buildStage
                image = Image.of("foo")
                interruptible = true
                resourceGroup = ResourceGroup.of("foo")
                timeout = 1.hours.plus(30.minutes)
                coverage = """/\d+.\d+ % covered/"""

                variables(mapOf("BAZ" to "foo"))
                needs(setup)
                cache(mvnCache)
                tags("bar")

                artifacts {
                    `when` = When.ALWAYS
                    expireIn = 21.days

                    paths(Path("foo"))

                    reports(
                        Artifacts.CoverageReport(Path("foo")),
                        Artifacts.JunitReport(listOf(Path("baz"))),
                    )
                }

                rules {
                    +rule {
                        `if` = "\$CI_PIPELINE_SOURCE == \"merge_request_event\""
                        allowFailure = true
                        exists = listOf("fo")
                        `when` = Rule.When.MANUAL
                        changes {
                            paths(listOf(Path("foo")))
                        }
                        variables("FOO" to "bar")
                    }
                    +onlyOnMrsRules
                }

                script(
                    """
                   echo "hello world";
                   echo "Welcome to GitLab CI/CD!"
                    """.trimIndent(),
                )
            }

            job("report") {
                stage = reportStage
                allowFailure = true

                script("echo \"hello world\"")
                variables("foo" to "bar")

                needs(unit)

                artifacts {
                    paths(Path("foo"))
                    reports(Artifacts.JunitReport(listOf(Path("foo"))))
                }

                tags(ownRunner, Tag("mytag"), Tag("otherTag"))

                rules(onlyOnMrRule)

                environment("coverage") {
                    deploymentTier = Environment.DeploymentTier.OTHER
                    url = "example.com"
                    action = Environment.Action.STOP
                    autoStopIn = 1.hours
                    onStop(unit)
                }
            }

            job("deploy") {
                stage = Stage("deploy")
                allowFailure = true

                script("echo \"hello world\"")
                variables("foo" to "bar")

                artifacts {
                    paths(listOf("foo", "bar"))
                }

                cache {
                    key = CacheKey.of(listOf(Path("./foo"), Path("./bar")), "hugo")
                    paths(".m2/repository")
                }

                needs(unit)

                tags(listOf("foo", "bar"))

                rules(onlyOnMrRule)

                environment("coverage") {
                    deploymentTier = Environment.DeploymentTier.PRODUCTION
                    url = "example.com"
                    action = Environment.Action.STOP
                    autoStopIn = 1.hours
                    onStop(unit)
                }
            }

            job("trigger") {
                stage = Stage("trigger")
                allowFailure = true

                trigger {
                    project = "fooo"
                    branch = "baz"
                    strategy = Trigger.Strategy.DEPEND
                    forward = Trigger.Forward(true, true)
                }
            }
        }

        expect.toMatchSnapshot(pipe.toYml())
    }
}
