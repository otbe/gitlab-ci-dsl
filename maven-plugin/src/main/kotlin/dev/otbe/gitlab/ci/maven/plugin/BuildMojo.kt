package dev.otbe.gitlab.ci.maven.plugin

import mu.KotlinLogging
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import java.io.FileReader
import java.nio.file.Files
import javax.inject.Inject
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import kotlin.io.path.Path
import kotlin.io.path.exists

private val logger = KotlinLogging.logger {}

@Mojo(name = "build", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
class BuildMojo @Inject constructor(
    @Parameter(defaultValue = "\${inputFile}", required = true)
    var inputFile: String,
    @Parameter(defaultValue = "\${outputFile}", required = true)
    var outputFile: String,
) : AbstractMojo() {
    override fun execute() {
        logger.info("Using input file: $inputFile")
        logger.info("Using output file: $outputFile")

        if (!Path(inputFile).exists()) {
            throw IllegalArgumentException("Input file does not exist!")
        }

        val se = getScriptEngine()
        val pipe = se.eval(FileReader(inputFile))
            ?: throw IllegalArgumentException("Failed to generate pipe! Check if your last line is similar to pipe.toYaml()")

        Files.write(Path(outputFile), pipe.toString().toByteArray())
    }

    private fun getScriptEngine(): ScriptEngine =
        ScriptEngineManager().getEngineByExtension("main.kts")!!
}
