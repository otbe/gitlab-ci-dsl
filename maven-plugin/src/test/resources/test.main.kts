#!/usr/bin/env kotlin

@file:DependsOn("dev.otbe:gitlab-ci-dsl:0.2.0")

import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.core.toYml
import dev.otbe.gitlab.ci.dsl.*

val pipe = pipeline {
    job("build") {
        image = Image.of("alpine:latest")

        scripts("echo 'Hello World'")
    }
}

pipe.toYml()
