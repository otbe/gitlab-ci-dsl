package dev.otbe.gitlab.ci.maven.plugin

import au.com.origin.snapshots.Expect
import au.com.origin.snapshots.junit5.SnapshotExtension
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import strikt.api.expectCatching
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import strikt.assertions.isFailure
import java.io.File

@ExtendWith(SnapshotExtension::class)
class BuildMojoTest {
    private lateinit var expect: Expect

    @Test
    fun `generate pipe`() {
        val output = File.createTempFile("pipe", ".yml")

        this::class.java.classLoader.getResource("test.main.kts")?.let {
            BuildMojo(it.path, output.path).execute()

            expect.toMatchSnapshot(output.readText())
        }
    }

    @Test
    fun `input file does not exist`() {
        expectCatching { BuildMojo("baz", "bar").execute() }
            .isFailure()
            .isA<IllegalArgumentException>()
            .get(Throwable::message).isEqualTo("Input file does not exist!")
    }

    @Test
    fun `somehow broken file`() {
        val output = File.createTempFile("pipe", ".yml")

        this::class.java.classLoader.getResource("broken.main.kts")?.let {
            expectCatching { BuildMojo(it.path, output.path).execute() }
                .isFailure()
                .isA<IllegalArgumentException>()
                .get(Throwable::message).isEqualTo("Failed to generate pipe! Check if your last line is similar to pipe.toYaml()")
        }
    }
}
