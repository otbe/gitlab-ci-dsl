package dev.otbe.gitlab.ci.constructs

import au.com.origin.snapshots.Expect
import au.com.origin.snapshots.junit5.SnapshotExtension
import dev.otbe.gitlab.ci.constructs.jobs.CodecovJob.codecovJob
import dev.otbe.gitlab.ci.constructs.jobs.MvnJob.mvnJob
import dev.otbe.gitlab.ci.constructs.jobs.MvnJob.mvnwJob
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.defaultPipeline
import dev.otbe.gitlab.ci.core.toYml
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(SnapshotExtension::class)
class ConstructsTest {
    private lateinit var expect: Expect

    @Test
    fun `example pipe mvn`() {
        val pipe = defaultPipeline {
            mvnJob("build", "install")
        }

        expect.toMatchSnapshot(pipe.toYml())
    }

    @Test
    fun `example pipe mvnw`() {
        val pipe = defaultPipeline {
            mvnwJob("build", "install")
        }

        expect.toMatchSnapshot(pipe.toYml())
    }

    @Test
    fun `example pipe codecov`() {
        val pipe = defaultPipeline {
            codecovJob()
        }

        expect.toMatchSnapshot(pipe.toYml())
    }

    @Test
    fun `exclude on schedule`() {
        val pipe = defaultPipeline {
            mvnJob("build", "install") {
                rules(DefaultPipeline.Rules.EXCLUDE_ON_SCHEDULE, DefaultPipeline.Rules.ALWAYS)
            }
        }

        expect.toMatchSnapshot(pipe.toYml())
    }
}
