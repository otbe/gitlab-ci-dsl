package dev.otbe.gitlab.ci.constructs.pipelines

import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.Rules.ONLY_ON_DEFAULT_BRANCH
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.defaultPipeline
import dev.otbe.gitlab.ci.core.model.Job
import dev.otbe.gitlab.ci.core.model.Stage
import dev.otbe.gitlab.ci.core.model.Workflow
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.first
import strikt.assertions.hasSize
import strikt.assertions.isEqualTo
import strikt.assertions.isNotNull

class DefaultPipelineTest {
    @Test
    fun `default pipeline test`() {
        val (_, workflow) = defaultPipeline {}

        expectThat(workflow)
            .isNotNull()
            .get(Workflow::rules)
            .isEqualTo(listOf(DefaultPipeline.Rules.MERGE_REQUESTS, DefaultPipeline.Rules.DEFAULT_BRANCH))
    }

    @Test
    fun `default pipe with job excluded for schedule`() {
        val pipe = defaultPipeline {
            job("foo") {
                stage = Stage("foo")
                rules(DefaultPipeline.Rules.EXCLUDE_ON_SCHEDULE, DefaultPipeline.Rules.ALWAYS)

                script("echo foo")
            }
        }

        expectThat(pipe.jobs)
            .hasSize(1)
            .first()
            .with(Job::rules) {
                hasSize(2)
                isEqualTo(listOf(DefaultPipeline.Rules.EXCLUDE_ON_SCHEDULE, DefaultPipeline.Rules.ALWAYS))
            }
    }

    @Test
    fun `default pipe with job only for default branch`() {
        val pipe = defaultPipeline {
            job("foo") {
                stage = Stage("foo")
                rules(ONLY_ON_DEFAULT_BRANCH)

                script("echo foo")
            }
        }

        expectThat(pipe.jobs)
            .hasSize(1)
            .first()
            .with(Job::rules) {
                hasSize(1)
                isEqualTo(listOf(ONLY_ON_DEFAULT_BRANCH))
            }
    }
}
