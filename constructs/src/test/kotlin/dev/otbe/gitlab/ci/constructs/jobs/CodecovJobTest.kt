package dev.otbe.gitlab.ci.constructs.jobs

import dev.otbe.gitlab.ci.core.model.Image
import dev.otbe.gitlab.ci.core.model.Job
import dev.otbe.gitlab.ci.core.model.Stage
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.hasSize
import strikt.assertions.isEqualTo

class CodecovJobTest {
    @Test
    fun codecovJobTest() {
        val codecovJob = CodecovJob.CodeCovJobBuilder().build()

        expectThat(codecovJob)
            .with(Job::name) {
                isEqualTo("codecov")
            }
            .with(Job::stage) {
                isEqualTo(Stage("codecov"))
            }
            .with(Job::image) {
                isEqualTo(Image.of("curlimages/curl"))
            }
            .with(Job::script) {
                hasSize(1)
                contains(
                    """
                    curl -Os https://uploader.codecov.io/latest/alpine/codecov
                    chmod +x codecov
                    ./codecov -t ${'$'}CODECOV_TOKEN
                    """.trimIndent(),
                )
            }
    }
}
