package dev.otbe.gitlab.ci.constructs.jobs

import dev.otbe.gitlab.ci.constructs.jobs.MvnJob.mvnCache
import dev.otbe.gitlab.ci.core.model.Image
import dev.otbe.gitlab.ci.core.model.Job
import dev.otbe.gitlab.ci.core.model.Stage
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.hasSize
import strikt.assertions.isEqualTo

class MvnJobTest {
    @Test
    fun mvnwJobTest() {
        val mvnwJob = MvnJob.MvnwJobBuilder("build", "clean install").build()

        expectThat(mvnwJob)
            .with(Job::stage) {
                isEqualTo(Stage("build"))
            }
            .with(Job::image) {
                isEqualTo(Image.of("eclipse-temurin:17.0.7_7-jdk"))
            }
            .with(Job::script) {
                hasSize(1)
                contains("./mvnw -Dmaven.repo.local=.m2/repository --batch-mode clean install")
            }
            .with(Job::cache) {
                isEqualTo(listOf(mvnCache))
            }
    }

    @Test
    fun mvnJobTest() {
        val mvnJob = MvnJob.MvnJobBuilder("build", "clean install").build()

        expectThat(mvnJob)
            .with(Job::stage) {
                isEqualTo(Stage("build"))
            }
            .with(Job::image) {
                isEqualTo(Image.of("maven:3.9.1-eclipse-temurin-17"))
            }
            .with(Job::script) {
                hasSize(1)
                contains("mvn -Dmaven.repo.local=.m2/repository --batch-mode clean install")
            }
            .with(Job::cache) {
                isEqualTo(listOf(mvnCache))
            }
    }
}
