package dev.otbe.gitlab.ci.constructs.jobs

import dev.otbe.gitlab.ci.core.model.Image
import dev.otbe.gitlab.ci.dsl.PipelineBuilder
import dev.otbe.gitlab.ci.dsl.jobs.JobBuilder

object CodecovJob {
    /**
     * Creates a job named "codecov" that uploads the coverage report to codecov.io.
     * You have to define a $CODECOV_TOKEN variable in your project in order to make this work.
     * Can be customized with [setup].
     */
    fun PipelineBuilder.codecovJob(name: String = "codecov", setup: JobBuilder.() -> Unit = {}) =
        job(CodeCovJobBuilder(name, setup).build())

    class CodeCovJobBuilder(name: String = "codecov", setup: JobBuilder.() -> Unit = {}) : JobBuilder(name) {
        init {
            image = Image.of("curlimages/curl")

            script(
                """
                curl -Os https://uploader.codecov.io/latest/alpine/codecov
                chmod +x codecov
                ./codecov -t ${'$'}CODECOV_TOKEN
                """.trimIndent(),
            )

            setup()
        }
    }
}
