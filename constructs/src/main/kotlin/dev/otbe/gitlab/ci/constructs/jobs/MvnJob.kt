package dev.otbe.gitlab.ci.constructs.jobs

import dev.otbe.gitlab.ci.core.model.Cache
import dev.otbe.gitlab.ci.core.model.Image
import dev.otbe.gitlab.ci.core.model.Path
import dev.otbe.gitlab.ci.dsl.PipelineBuilder
import dev.otbe.gitlab.ci.dsl.cache.CacheBuilder
import dev.otbe.gitlab.ci.dsl.jobs.JobBuilder

object MvnJob {
    /**
     * Creates a maven based job.
     *
     * Via [cmd] you can define the maven command to run.
     * All executed commands with this job will cache (pull-push) the maven repository automatically.
     * Standard image to be used is: maven:3.9.1-eclipse-temurin-17
     *
     * Can be customized with [setup].
     */
    fun PipelineBuilder.mvnJob(name: String, cmd: String, setup: JobBuilder.() -> Unit = {}) =
        job(MvnJobBuilder(name, cmd, setup).build())

    /**
     * Creates a maven based job and uses the project local maven wrapper to execute goals.
     *
     * Via [cmd] you can define the maven command to run.
     * All executed commands with this job will cache (pull-push) the maven repository automatically.
     * Standard image to be used is: eclipse-temurin:17.0.7_7-jdk
     *
     * Can be customized with [setup].
     */
    fun PipelineBuilder.mvnwJob(name: String, cmd: String, setup: JobBuilder.() -> Unit = {}) =
        job(MvnwJobBuilder(name, cmd, setup).build())

    class MvnJobBuilder(name: String, cmd: String, setup: JobBuilder.() -> Unit = {}) : JobBuilder(name) {
        init {
            image = Image.of("maven:3.9.1-eclipse-temurin-17")

            cache(mvnCache)

            script(mvn(cmd))

            setup()
        }
    }

    class MvnwJobBuilder(name: String, cmd: String, setup: JobBuilder.() -> Unit = {}) : JobBuilder(name) {
        init {
            image = Image.of("eclipse-temurin:17.0.7_7-jdk")

            cache(mvnCache)

            script(mvnw(cmd))

            setup()
        }
    }

    val mvnCache = CacheBuilder {
        policy = Cache.Policy.PULL_PUSH
        paths(Path(".m2/"))
    }.build()

    fun mvnw(command: String): String {
        return "./mvnw -Dmaven.repo.local=.m2/repository --batch-mode $command"
    }

    fun mvn(command: String): String {
        return "mvn -Dmaven.repo.local=.m2/repository --batch-mode $command"
    }
}
