package dev.otbe.gitlab.ci.constructs.pipelines

import dev.otbe.gitlab.ci.core.model.Rule
import dev.otbe.gitlab.ci.dsl.PipelineBuilder

object DefaultPipeline {
    /**
     * Creates a default pipeline and only creates a pipeline for:
     * - merge requests
     * - default branch
     *
     * Can be customized with [setup].
     * You can customize single jobs by using rules defined in `DefaultPipeline.Rules`.
     */
    fun defaultPipeline(setup: PipelineBuilder.() -> Unit) =
        PipelineBuilder {
            workflow(listOf(Rules.MERGE_REQUESTS, Rules.DEFAULT_BRANCH))
        }.also(setup).build()

    object Rules {
        val MERGE_REQUESTS = Rule("\$CI_PIPELINE_SOURCE == \"merge_request_event\"")
        val DEFAULT_BRANCH = Rule("\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH")
        val ONLY_ON_DEFAULT_BRANCH = Rule("\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH")
        val EXCLUDE_ON_SCHEDULE = Rule("\$CI_PIPELINE_SOURCE == \"schedule\"", `when` = Rule.When.NEVER)
        val ALWAYS = Rule(`when` = Rule.When.ALWAYS)
    }
}
