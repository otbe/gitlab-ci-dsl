@file:DependsOn("dev.otbe:gitlab-ci-constructs:0.2.0")

import dev.otbe.gitlab.ci.constructs.jobs.CodecovJob.codecovJob
import dev.otbe.gitlab.ci.constructs.jobs.MvnJob.mvnwJob
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.Rules.ALWAYS
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.Rules.EXCLUDE_ON_SCHEDULE
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.Rules.ONLY_ON_DEFAULT_BRANCH
import dev.otbe.gitlab.ci.constructs.pipelines.DefaultPipeline.defaultPipeline
import dev.otbe.gitlab.ci.core.model.*
import dev.otbe.gitlab.ci.core.toYml

val buildStage = Stage("build")
val deployStage = Stage("deploy")
val pipe = defaultPipeline {
    include(Include.from("renovate-bot/renovate-runner", "/templates/renovate.gitlab-ci.yml"))

    mvnwJob("build", "install") {
        stage = buildStage
        rules(EXCLUDE_ON_SCHEDULE, ALWAYS)

        artifacts {
            `when` = When.ALWAYS

            paths("coverage/target/site/jacoco-aggregate/")

            reports(Artifacts.JunitReport(listOf(Path("**/target/surefire-reports/TEST-*.xml"))))
        }
    }

    codecovJob {
        stage = deployStage
        rules(EXCLUDE_ON_SCHEDULE, ALWAYS)
    }

    job("pages") {
        stage = deployStage
        rules(EXCLUDE_ON_SCHEDULE, ONLY_ON_DEFAULT_BRANCH)

        script(
            """
                mv docs/ public/
            """.trimIndent(),
        )

        artifacts {
            paths("public")
        }
    }
}

pipe.toYml()
