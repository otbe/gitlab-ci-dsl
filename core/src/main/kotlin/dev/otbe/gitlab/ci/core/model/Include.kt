package dev.otbe.gitlab.ci.core.model

import dev.otbe.gitlab.ci.core.filterNotNullValues
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = MapSerializer::class)
open class Include(
    val include: Map<String, String?> = mutableMapOf(),
) {
    companion object {
        fun from(project: String, file: String) = IncludeProject(project, file)
    }
}

object MapSerializer : KSerializer<Include> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("name", PrimitiveKind.STRING)

    private val mapSerializer = MapSerializer(String.serializer(), String.serializer())

    override fun deserialize(decoder: Decoder): Include {
        TODO("Not yet implemented")
    }

    override fun serialize(encoder: Encoder, value: Include) {
        mapSerializer.serialize(encoder, value.include.filterNotNullValues())
    }
}

class IncludeProject(
    project: String,
    file: String,
    ref: String? = null,
) : Include(
    mapOf(
        "project" to project,
        "file" to file,
        "ref" to ref,
    ),
)
