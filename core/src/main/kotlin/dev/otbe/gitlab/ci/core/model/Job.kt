package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.*
import kotlin.time.Duration

@Serializable
data class Job(
    @Transient val name: String = "",
    val stage: Stage,
    val image: Image? = null,
    val variables: Map<String, String>? = emptyMap(),
    val needs: List<String> = emptyList(),
    val artifacts: Artifacts? = null,
    val rules: List<Rule> = emptyList(),
    val script: List<String> = emptyList(),
    val cache: List<Cache>? = emptyList(),
    val coverage: String? = null,
    val interruptible: Boolean? = false,
    @SerialName("allow_failure")
    val allowFailure: Boolean? = false,
    @Serializable(with = CustomDurationSerializer::class)
    val timeout: Duration? = null,
    @SerialName("resource_group")
    val resourceGroup: ResourceGroup? = null,
    val tags: List<Tag>? = emptyList(),
    val environment: Environment? = null,
    val trigger: Trigger? = null,
)
