package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.Serializable

@Serializable(with = StageSerializer::class)
data class Stage(val name: String)

object StageSerializer : SingleValueSerializer<Stage>() {
    override fun extractValue(value: Stage): String =
        value.name
}
