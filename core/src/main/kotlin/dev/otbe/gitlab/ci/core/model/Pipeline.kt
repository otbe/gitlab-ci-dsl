package dev.otbe.gitlab.ci.core.model

data class Pipeline(
    val includes: List<Include>? = emptyList(),
    val workflow: Workflow? = null,
    val defaults: String? = null,
    val stages: List<Stage> = emptyList(),
    val jobs: List<Job> = emptyList(),
)
