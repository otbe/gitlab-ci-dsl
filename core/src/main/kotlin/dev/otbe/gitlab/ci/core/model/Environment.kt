package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.time.Duration

@Serializable
data class Environment(
    val name: String,
    val url: String? = null,
    @SerialName("on_stop")
    val onStop: String? = null,
    val action: Action? = null,
    @SerialName("auto_stop_in")
    @Serializable(with = CustomDurationSerializer::class)
    val autoStopIn: Duration? = null,
    @SerialName("deployment_tier")
    val deploymentTier: DeploymentTier? = null,
) {

    enum class Action {
        @SerialName("start")
        START,

        @SerialName("prepare")
        PREPARE,

        @SerialName("stop")
        STOP,

        @SerialName("verify")
        VERIFY,

        @SerialName("access")
        ACCESS,
    }

    enum class DeploymentTier {
        @SerialName("production")
        PRODUCTION,

        @SerialName("staging")
        STAGING,

        @SerialName("testing")
        TESTING,

        @SerialName("development")
        DEVELOPMENT,

        @SerialName("other")
        OTHER,
    }
}
