package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.Serializable

@Serializable
class Workflow(
    val name: String? = null,
    val rules: List<Rule> = emptyList(),
)
