package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

abstract class SingleValueSerializer<T> : KSerializer<T> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("name", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): T {
        TODO("Not yet implemented")
    }

    override fun serialize(encoder: Encoder, value: T) {
        encoder.encodeString(extractValue(value))
    }

    abstract fun extractValue(value: T): String
}
