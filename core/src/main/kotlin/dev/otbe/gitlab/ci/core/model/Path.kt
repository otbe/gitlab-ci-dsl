package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.Serializable

@Serializable(with = PathSerializer::class)
data class Path(val name: String)

object PathSerializer : SingleValueSerializer<Path>() {
    override fun extractValue(value: Path): String =
        value.name
}
