package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.Serializable

@Serializable
data class Image(val name: String, val entrypoint: String? = null) {
    companion object {
        fun of(name: String) = Image(name)

        fun of(name: String, entrypoint: String?) = Image(name, entrypoint)
    }
}
