package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// include currently not supported
@Serializable
data class Trigger(
    val project: String? = null,
    val branch: String? = null,
    val strategy: Strategy? = null,
    val forward: Forward? = null,
) {
    enum class Strategy {
        @SerialName("depend")
        DEPEND,
    }

    @Serializable
    data class Forward(
        @SerialName("yaml_variables")
        val yamlVariables: Boolean? = null,
        @SerialName("pipeline_variables")
        val pipelineVariables: Boolean? = null,
    )
}
