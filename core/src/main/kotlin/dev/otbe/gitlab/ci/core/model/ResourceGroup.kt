package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.Serializable

@Serializable(with = ResourceGroupSerializer::class)
data class ResourceGroup(val name: String) {
    companion object {
        fun of(name: String) = ResourceGroup(name)
    }
}

object ResourceGroupSerializer : SingleValueSerializer<ResourceGroup>() {
    override fun extractValue(value: ResourceGroup): String =
        value.name
}
