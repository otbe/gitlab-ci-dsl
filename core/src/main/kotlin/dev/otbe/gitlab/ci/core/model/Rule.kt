package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Rule(
    val `if`: String? = null,
    val changes: Change? = null,
    val exists: List<String> = emptyList(),
    @SerialName("allow_failure")
    val allowFailure: Boolean? = null,
    val variables: Map<String, String>? = emptyMap(),
    val `when`: When? = null,
) {
    enum class When {
        @SerialName("on_success")
        ON_SUCCESS,

        @SerialName("manual")
        MANUAL,

        @SerialName("never")
        NEVER,

        @SerialName("delayed")
        DELAYED,

        @SerialName("always")
        ALWAYS,
    }

    @Serializable
    data class Change(val paths: List<Path>, @SerialName("compare_to") val compareTo: List<String> = emptyList())
}
