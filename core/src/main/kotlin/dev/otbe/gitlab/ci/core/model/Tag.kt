package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.Serializable

@Serializable(with = TagSerializer::class)
data class Tag(val name: String) {
    companion object {
        fun of(name: String) = Tag(name)
    }
}

object TagSerializer : SingleValueSerializer<Tag>() {
    override fun extractValue(value: Tag): String =
        value.name
}
