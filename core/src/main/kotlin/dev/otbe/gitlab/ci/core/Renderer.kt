package dev.otbe.gitlab.ci.core

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import dev.otbe.gitlab.ci.core.model.Include
import dev.otbe.gitlab.ci.core.model.Job
import dev.otbe.gitlab.ci.core.model.Pipeline
import dev.otbe.gitlab.ci.core.model.Workflow
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.add
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.putJsonArray
import java.nio.file.Files
import kotlin.io.path.Path

@OptIn(ExperimentalSerializationApi::class)
internal val json = Json { explicitNulls = false }

private val jsonMapper = ObjectMapper()
private val yamlMapper = YAMLMapper()
    .configure(YAMLGenerator.Feature.INDENT_ARRAYS, true)
    .configure(YAMLGenerator.Feature.INDENT_ARRAYS_WITH_INDICATOR, true)
    .configure(YAMLGenerator.Feature.MINIMIZE_QUOTES, true)
    .configure(YAMLGenerator.Feature.WRITE_DOC_START_MARKER, false)
    .configure(YAMLGenerator.Feature.SPLIT_LINES, true)

internal fun Pipeline.toJson() =
    buildJsonObject {
        includes?.also { includes ->
            if (includes.isNotEmpty()) {
                putJsonArray("include") {
                    includes.forEach { add(json.encodeToJsonElement(Include.serializer(), it)) }
                }
            }
        }

        workflow?.also {
            put("workflow", json.encodeToJsonElement(Workflow.serializer(), it))
        }

//            defaults?.also { put() }

        putJsonArray("stages") {
            stages.forEach { add(it.name) }
        }

        jobs.associateBy { it.name }.forEach {
            put(it.key, json.encodeToJsonElement(Job.serializer(), it.value))
        }
    }

fun Pipeline.toYml(): String {
    val node = jsonMapper.readTree(toJson().toString())
    return yamlMapper.writeValueAsString(node)
}

infix fun Pipeline.goesTo(target: String) {
    Files.write(Path(target), toYml().lines())
}
