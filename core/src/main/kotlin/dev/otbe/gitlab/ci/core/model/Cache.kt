package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.*
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
data class Cache(
    val paths: List<Path> = emptyList(),
    val key: CacheKey? = null,
    val untracked: Boolean? = false,
    val unprotect: Boolean? = false,
    val `when`: When? = When.ON_SUCCESS,
    val policy: Policy? = Policy.PULL_PUSH,
    @SerialName("fallback_keys")
    val fallbackKeys: List<String>? = emptyList(),
) {

    enum class Policy {
        @SerialName("pull")
        PULL,

        @SerialName("push")
        PUSH,

        @SerialName("pull-push")
        PULL_PUSH,
    }
}

@Serializable(with = CacheKeySerializer::class)
interface CacheKey {
    companion object {
        fun of(key: String) = SimpleCacheKey(key)
        fun of(files: List<Path>, prefix: String? = null) = ComplexCacheKey(files, prefix)
    }
}

data class SimpleCacheKey(
    val key: String,
) : CacheKey

data class ComplexCacheKey(
    val files: List<Path>,
    val prefix: String? = null,
) : CacheKey

@OptIn(InternalSerializationApi::class)
object CacheKeySerializer : KSerializer<CacheKey> {
    @OptIn(ExperimentalSerializationApi::class)
    override val descriptor: SerialDescriptor = buildSerialDescriptor("name", StructureKind.OBJECT) {
        element("prefix", String.serializer().descriptor)
        element("files", buildSerialDescriptor("files", StructureKind.LIST))
    }

    override fun deserialize(decoder: Decoder): CacheKey {
        TODO("Not yet implemented")
    }

    override fun serialize(encoder: Encoder, value: CacheKey) {
        when (value) {
            is SimpleCacheKey -> encoder.encodeString(value.key)
            is ComplexCacheKey -> {
                encoder.beginStructure(descriptor).apply {
                    value.prefix?.let {
                        encodeStringElement(descriptor, 0, value.prefix)
                    }
                    encodeSerializableElement(descriptor, 1, ListSerializer(Path.serializer()), value.files)
                    endStructure(descriptor)
                }
            }
        }
    }
}
