package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.*
import kotlinx.serialization.json.*
import kotlin.time.Duration

@Serializable
data class Artifacts(
    val name: String? = null,
    val public: Boolean? = null,
    @SerialName("expire_in")
    @Serializable(with = CustomDurationSerializer::class)
    val expireIn: Duration? = null,
    @SerialName("expose_as")
    val exposeAs: String? = null,
    val paths: List<Path>? = emptyList(),
    @Transient
    val reports: List<Report> = emptyList(),
    val untracked: Boolean? = null,
    val `when`: When? = When.ON_SUCCESS,
    @SerialName("reports")
    var r: JsonObject? = null,
) {
    init {
        if (reports.isNotEmpty()) {
            r = buildJsonObject {
                reports.forEach { it.build(this) }
            }
        }
    }

    sealed class Report {
        abstract fun build(builder: JsonObjectBuilder)
    }

    class CoverageReport(
        private val path: Path,
        private val coverageFormat: String = "cobertura",
    ) : Report() {
        override fun build(builder: JsonObjectBuilder) {
            builder.apply {
                put(
                    "coverage_report",
                    buildJsonObject {
                        put("path", path.name)
                        put("coverage_format", coverageFormat)
                    },
                )
            }
        }
    }

    class JunitReport(
        private val path: List<Path>,
    ) : Report() {
        override fun build(builder: JsonObjectBuilder) {
            builder.apply {
                putJsonArray("junit") {
                    path.forEach { add(it.name) }
                }
            }
        }
    }

    // TODO finish other reports
}
