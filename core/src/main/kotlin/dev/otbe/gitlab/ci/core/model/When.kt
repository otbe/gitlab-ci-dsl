package dev.otbe.gitlab.ci.core.model

import kotlinx.serialization.SerialName

enum class When {
    @SerialName("on_success")
    ON_SUCCESS,

    @SerialName("on_failure")
    ON_FAILURE,

    @SerialName("always")
    ALWAYS,
}
