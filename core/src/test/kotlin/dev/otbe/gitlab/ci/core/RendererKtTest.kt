package dev.otbe.gitlab.ci.core

import au.com.origin.snapshots.Expect
import au.com.origin.snapshots.junit5.SnapshotExtension
import dev.otbe.gitlab.ci.core.model.Pipeline
import dev.otbe.gitlab.ci.core.model.Stage
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.File

@ExtendWith(SnapshotExtension::class)
class RendererKtTest {
    lateinit var expect: Expect

    @Test
    fun `pipe goesTo tmp`() {
        val tmpFile = File.createTempFile("pipe", ".yml")

        Pipeline(stages = listOf(Stage("bar"))) goesTo tmpFile.path

        expect.toMatchSnapshot(tmpFile.readText())
    }
}
